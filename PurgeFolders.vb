Imports System.Configuration
Imports System.IO
Imports System.configuration.ConfigurationSettings
Imports System.xml
Imports ntb_FuncLib

Module PurgeFolders

    Dim _logpath As String
    Dim _fileWait As Integer
    Dim _xmlConfig As XmlDocument = New XmlDocument

    Sub Init()

        'Load settings
        _logpath = AppSettings("LogPath")
        _fileWait = AppSettings("FileWaitSleep")

        Try
            _xmlConfig.Load(ConfigurationManager.AppSettings("ConfigFile"))
        Catch ex As Exception
            LogFile.WriteErr(_logpath, "Failed to load config XML file.", ex)
        End Try

    End Sub

    Sub Main()

        Dim list As XmlNodeList
        Dim node As XmlNode

        'Settings
        Init()

        list = _xmlConfig.SelectNodes("purge/path")

        For Each node In list

            Dim id As Integer = 0
            Dim disabled As Boolean = False
            Dim recurse As Boolean = False
            Dim purgedays As Integer = 0

            Dim folder As String = ""
            Dim filter As String = ""
            Dim excludeString As String = ""

            Try
                id = node.Attributes("id").Value
                folder = node.InnerText
                disabled = (node.Attributes("disabled").Value.ToLower = "true")
                recurse = (node.Attributes("recurse").Value.ToLower = "true")
                purgedays = node.Attributes("days").Value

                Try
                    filter = node.Attributes("filter").Value
                    excludeString = node.Attributes("exclude").Value
                Catch ex As Exception
                    filter = "*.*"
                    excludeString = ""
                End Try

            Catch ex As Exception
                LogFile.WriteErr(_logpath, "Error in config XML. Job ID: " & id, ex)
            End Try

            If Not disabled And purgedays > 0 Then
                LogFile.WriteLogNoDate(_logpath, "--------------------------------------------------------------------")
                LogFile.WriteLog(_logpath, "Start job " & id & ": " & folder)
                PurgeFolderRecurse(folder, purgedays, recurse, filter, excludeString)
            End If

        Next


    End Sub


    'Main purge function, recursive
    Sub PurgeFolderRecurse(ByVal folder As String, ByVal purgeDays As Integer, Optional ByVal recurse As Boolean = True, Optional ByVal filter As String = "*.*", Optional ByVal exclude As String = "")

        Dim f, dir As String
        Dim files As String()
        Dim folders As String()
        Dim td As Date = Today.AddDays(-purgeDays)

        'Get files
        files = Directory.GetFiles(folder, filter)

        Threading.Thread.Sleep(_fileWait * 1000)

        'Loop and purge
        For Each f In files

            Try
                'Get DATE only
                Dim dt As Date = File.GetLastWriteTime(f).ToString("yyyy-MM-dd")

                'Compare and delete
                If dt <= td And (f.IndexOf(exclude, StringComparison.Ordinal) = -1 Or exclude = "") Then
                    'Delete
                    File.Delete(f)

                    'Log
                    LogFile.WriteLog(_logpath, "Deleted: " & f)
                End If
            Catch ex As Exception
                LogFile.WriteLog(_logpath, "Failed: " & f)
                LogFile.WriteErr(_logpath, "Failed to delete file: " & f, ex)
            End Try
        Next

        If recurse Then
            'GetFolders
            folders = Directory.GetDirectories(folder)

            'Loop and recurse
            For Each dir In folders
                PurgeFolderRecurse(dir, purgeDays, recurse)
            Next
        End If

    End Sub

End Module
